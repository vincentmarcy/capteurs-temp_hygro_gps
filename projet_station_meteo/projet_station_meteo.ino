//invite la bibliothèque DHT
#include "DHT.h"
#define DHTPIN 12 //identifie la sortie du capteur
#define DHTTYPE DHT11 //identifie le type de capteur
//creer l'objet sensor de type DHT:
DHT sensor(DHTPIN, DHTTYPE);
#include <SevSeg.h>  //invite la bibliothèque SevSeg qui transforme votre Arduino en un contrôleur d'affichage à sept segments
SevSeg sevseg;
long MAJ; //declarer MAJ comme variable ...
float h, t; //declarer h:humidité et t:temperature comme variable décimale
int aff = 0; ///0: temp   1:humidity
void setup() {
 byte numDigits = 4;
 pinMode(1, OUTPUT);
 byte digitPins[] = {10, 11, 0, 13};
 byte segmentPins[] = {9, 2, 3, 5, 6, 8, 7, 4};
 bool resistorsOnSegments = true;
 byte hardwareConfig = COMMON_ANODE;
 MAJ = millis();
 sevseg.begin(hardwareConfig, numDigits, digitPins, segmentPins, resistorsOnSegments);
 //Serial.begin(9600);
 sensor.begin();
}
void loop() {
 //delay(1000);
 static unsigned long timer = millis();
 if (millis() - MAJ > 5000) {
   MAJ = millis();
   h = sensor.readHumidity();
   t = sensor.readTemperature();
   //Serial.print("H:");
   //Serial.print(h);
   //Serial.print(",");
   //Serial.print("T:");
   //Serial.println(t);
   if (aff == 0) {
     aff = 1;
     digitalWrite(1, LOW);
   } else {
     aff = 0;
     digitalWrite(1, HIGH);
   }
 }
 if (aff == 0) {
   sevseg.setNumber(t, 1);
   sevseg.refreshDisplay();
 } else {
   sevseg.setNumber(h, 1);
   sevseg.refreshDisplay();
 }
}
